# Nginx
#
# VERSION               0.0.1

FROM ubuntu:14.04
MAINTAINER Calvin Pham <calvin@inboxs.com>

LABEL Description="This image is used to run webapp" Version="1.0"

# ADD ./repo/nginx.repo /etc/yum.repos.d/

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
# RUN yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm

# enable php7
RUN yum clean all
RUN yum makecache fast
RUN yum install -y nginx
RUN service nginx start
